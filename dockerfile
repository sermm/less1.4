FROM testnginx:v1

RUN apt update && apt install -y tar wget procps gcc libghc-regex-pcre-dev zlib1g-dev build-essential
RUN echo "Update complete"
WORKDIR /app
RUN wget http://nginx.org/download/nginx-1.18.0.tar.gz
RUN tar xvfz nginx-1.18.0.tar.gz
RUN cd /app/nginx-1.18.0 && ./configure && make && make install

FROM debian:9
WORKDIR /usr/local/nginx/sbin
COPY --from=0 /usr/local/nginx/sbin/nginx .
RUN mkdir /usr/local/nginx/logs/
RUN touch /usr/local/nginx/logs/error.log
CMD ["./nginx", "-g" , "daemon off;"]




